# Preface:

- This data model includes extensions of the ManuscriptVersion model specifically required by eLife.
- This data model is based on current xpub-collabra and xpub-faraday models, the discussion that was held in Athens by teams working on those projects, and the following discussion held in Cambridge with those teams as well as those working on xpub-elife.
- These models do not use Collections and Fragments, instead they are standalone models in separate tables.
- JATS was used as a vocabulary, wherever appropriate.
- JATS was used as a source of data types, wherever appropriate.
- All groups of users are modelled with Teams (e.g. editors, reviewers, etc.)
- All models have potentially semantic implicit 'created' and 'updated' dates
- All models have an implicit 'id' UUID 
- All dates are ISO-8601 strings, as PostgreSQL recommends this as date input, JATS allows it, and GraphQL can handle it.

# Models

##  Journal
**NOT USED** - The repository should only contain eLife journal entries

##  Manuscript
Extension of the PubSweet entity.

|   | type  | JATS  | comment  |  
|---|---|---|---|
| Stage | Stage | | Enumeration |
| Cover Letter | string | | Probabaly should have a mimetype |
| Title | string |
| Type | SubmissionType |
| Article ID | string | | Our 5 digit one. Not thought about multiple IDs|
| Subject Areas | Major Subject Area[ ] | | Enumeration, max 2|
| Previously Discussed | string | | |
| Previously Submitted | string | | |
| Research Advance | string | | |
| Scientific Correspondence | string | | |
| co-Submission | string [ ] | | |
| Related Submissions | Related Submission [ ] | | This is verified by staff from any of previously submitted, co-submission, advance or scientific correspondence|
| Suggested Editors | Editor Suggestion [ ] | | |
| Excluded Editors | Editor Suggestion [ ] | | |
| Suggested Reviewers | Reviewer Suggestion [ ] | | |
| Excluded Reviewers | Reviewer Suggestion [ ] | | |
| Declaration COI | boolean | | |
| Persons | Manuscript Person [ ] | | |
| QC Issues | Issue |
| Files | File [ ] |
| History | ChangeLog [ ] |

## User 
Extension of the PubSweet entity.

|   | type  | JATS  | comment  |  
|---|---|---|---|
| firstName | String!|
| lastName | String!|
| publishedName | String|
| institution | String!|
| email | String!|
| orcidId | String!|
| roles | [GlobalRole!]!|

## UserAlias
|   | type  | JATS  | comment  |  
|---|---|---|---|
| firstName | String!|
| lastName | String!|
| publishedName | String|
| email | String!|
| institution | String!|

## Manuscript Person
|   | type  | JATS  | comment  |  
|---|---|---|---|
|User| User? |
|Permissions |
|Role| Manuscript Role |
|Alias| Alias? |
|Metadata| Manuscript Person Metadata |


## Author Metadata : Manuscript Person Metadata
|   | type  | JATS  | comment  |  
|---|---|---|---|
|Rank| Integer |
|Confirmed| boolean |
|IsCorresponding | boolean |
|Co-Relationship| Manuscript Person [ ] |
|Contributions| Contribution [ ]  |
|Conflict of Interest| string |


## Reviewer Metadata : Manuscript Person Metadata
|   | type  | JATS  | comment  |  
|---|---|---|---|
|Rank | Integer |
|Co-Reviewer | Manuscript Person [ ] | | More than one reviewer behind the scenes |
|Conflict of Interest | string |
|Reveal Identity | boolean |


## Contribution (enum)
|   | type  | JATS  | comment  |  
|---|---|---|---|
| CREDiT
| Free Text
 
## Global Role (enum)

I think there is an argument that this could be `Team` as they are user groups.

|   | type  | JATS  | comment  |  
|---|---|---|---|
|STAFF |
|EDITORINCHIEF |
|DEPUTYEDITOR |
|SENIOREDITOR |
|REVIEWINGEDITOR |


## Manuscript Role (enum)
|   | type  | JATS  | comment  |  
|---|---|---|---|
|   DEPUTYEDITOR |
|   SENIOREDITOR |
|   REVIEWINGEDITOR |
|   REVIEWER |
|   AUTHOR |
|   SUBMITTER |


## Editor Suggestion
|   | type  | JATS  | comment  |  
|---|---|---|---|
| User | User |
| Role | Manuscript Role |
| Reason | String |


## Reviewer Suggestion
|   | type  | JATS  | comment  |  
|---|---|---|---|
| Name | String |
| Email | String |
| Role | String |
| Reason | String  |


## Alias
|   | type  | JATS  | comment  |  
|---|---|---|---|
| First name | string |
| Last name | string |
| Email |  string |
| Institution |  string |

## Major Subject Area (enum)
|   | type  | JATS  | comment  |  
|---|---|---|---|
| BIOCHEMISTRYANDCHEMICALBIOLOGY |
| CANCERBIOLOGY |
| CELLBIOLOGY |
| CHROMOSOMESANDGENEEXPRESSION |
| COMPUTATIONALANDSYSTEMSBIOLOGY |
| DEVELOPMENTALBIOLOGYANDSTEMCELLS |
| ECOLOGY |
| EPIDEMIOLOGYANDGLOBALHEALTH |
| EVOLUTIONARYBIOLOGY |
| GENETICSANDGENOMICS |
| HUMANBIOLOGYANDMEDICINE |
| IMMUNOLOGYANDINFLAMMATION |
| MICROBIOLOGYANDINFECTIOUSDISEASE |
| NEUROSCIENCE |
| PHYSICSOFLIVINGSYSTEMS |
| PLANTBIOLOGY |
| STRUCTURALBIOLOGYANDMOLECULARBIOPHYSICS |

## Change Log
|   | type  | JATS  | comment  |  
|---|---|---|---|
| Manuscript | Manuscript |
| Timestamp | DateTime |
| User | User |
| Actions | Action [ ] |

## Action
TBD 

## File
|   | type  | JATS  | comment  |  
|---|---|---|---|
| URI | string |
| Type | string |
| Mime Type | string |
| size | string |
| File type | string|

This does have lastChangedUser/Date on in our schema - why don't we use the ChangeLog?

## File Type
|   | type  | JATS  | comment  |  
|---|---|---|---|
| ARTICLEFILE |
| FIGURE |
| FIGURESUPPLEMENT |
| SOURCEDATA |
| RICHMEDIA |
| SOURCECODE |
| SUPPLEMENTARYFILE |


## Related Submission

This is a verified relation - during QC a member of staff will link the two.

|   | type  | JATS  | comment  |  
|---|---|---|---|
| Link Type | Submission Relationship |
| Related Manuscript | String | articleIdentifier |


## SubmissionRelationship
|   | type  | JATS  | comment  |  
|---|---|---|---|
| COSUBMISSION |
| RESUBMISSION |
| ADVANCE |
| SCIENTIFICCORRESPONDENCE |

## User
|   | type  | JATS  | comment  |  
|---|---|---|---|
| Orcid | ORCID |
| Keywords | String [ ] |
| Role | Global Role [ ] |

## Stage
|   | type  | JATS  | comment  |  
|---|---|---|---|
| STARTED |
| SUBMITTED |
| AWAITINGINITIALQUALITYCHECK |
| INITIALQUALITYCHECKINPROGRESS |
| CHANGESREQUESTED |
| AWAITINGCHANGES |
| CHANGESCOMPLETE |
| INITIALQUALITYCHECKCOMPLETE |
| DEPUTYEDITORASSIGNED |
| AWAITINGDEPUTYEDITORASSESSMENT |
| DEPUTYEDITORASSESSMENTINPROGRESS |
| DEPUTYEDITORASSESSMENTCOMPLETE |
| SENIOREDITORASSIGNED |
| AWAITINGSENIOREDITORASSESSMENT |
| SENIOREDITORASSESSMENTINPROGRESS |
| SENIOREDITORASSESSMENTCOMPLETE |
| DECISIONLETTERDRAFTED |
| ENCOURAGED |
| REJECTED |
| WITHDRAWN |

## Location
|   | type  | JATS  | comment  |  
|---|---|---|---|
|Location identifier | string | |  Could be JATS Section|

## QC Issue
|   | type  | JATS  | comment  |  
|---|---|---|---|
|Manuscript | Manuscript |
|Location | Location |
|Comments | Comment [ ] |
|Type | String |
|Resolved | Boolean |

## Comment
|   | type  | JATS  | comment  |  
|---|---|---|---|
|Raised by | User |
|Comment | String |


## Submission Type
|   | type  | JATS  | comment  |  
|---|---|---|---|
| RESEARCHARTICLE |
| TOOLSANDRESOURCES |
| SHORTREPORT |
| RESEARCHADVANCE |
| FEATUREARTICLE |
| SCIENTIFICCORRESPONDENCE |
