# Preface:

- This data model is based on current xpub-collabra and xpub-faraday models, the discussion that was held in Athens by teams working on those projects, and the following discussion held in Cambridge with those teams as well as those working on xpub-elife.
- These models do not use Collections and Fragments, instead they are standalone models in separate tables.
- JATS was used as a vocabulary, wherever appropriate.
- JATS was used as a source of data types, wherever appropriate.
- All groups of users are modelled with Teams (e.g. editors, reviewers, etc.)
- All models have potentially semantic implicit 'created' and 'updated' dates
- All models have an implicit 'id' UUID 
- All dates are ISO-8601 strings, as PostgreSQL recommends this as date input, JATS allows it, and GraphQL can handle it.

# Models

##  Journal

|   | type  | JATS  | comment  |  
|---|---|---|---|
| journalTitle  | String  | `<journal-title>`  |   |
| manuscripts  | [Manuscript]  |   |  |
| meta | JournalMetadata | | 
|  └ meta.publisherName | String | `<publisher-name>` |
|  └ meta.issn | String | `<issn>` | |

##  Manuscript

|   | type  | JATS  | comment  |  
|---|---|---|---|
| versions  | [ManuscriptVersion]  |   | Link to ManuscriptVersions  |


## ManuscriptVersion

|   | type  | JATS  | comment  |  
|---|---|---|---|
| submissionDate  | String  |   | date  |
| status  | String  |   | e.g. `initial` or `QA`   |
| reviewers  | Team -> [User]  |   | Link to the reviewers team's members |
| files  | [Files]  | `<graphic>`, `<media>`,`<supplementary-material>` | |
| suggestedEditors | [Suggestion] |  | |
|  └ suggestion.name | String | | e.g. 'Mr. Opposition' |
|  └ suggestion.type | String | | e.g. 'seniorEditor' |
|  └ suggestion.reason | String | | e.g. 'They oppose me' |
| opposedEditors | [Suggestion] | | same structure as above |
| suggestedReviewers | [Suggestion] | | same structure as above |
| opposedReviewers | [Suggestion] | | same structure as above |
| reviews | [Review] | | |
| recommendations  | [Recommendation]  |   |  Link to the version's reviews' recommentations |
| decision  | Decision  |   |   |
| articleType | String | `@article-type` | |
| conflict | String | `<notes notes-type="conflict-interest">` | |
| meta  | ManuscriptMetadata  |   |   |
|  └ meta.articleIds | [Article IDs] | `<article-id>` | |
|  &nbsp;&nbsp;└ articleId.type | String | `@pub-id-type` | doi, pmid, etc. |
|  &nbsp;&nbsp;└ articleId.id | String | | |
|  └ meta.title | String | `<title>` | |
|  └ meta.abstract | String | `<abstract>` | |
|  └ meta.coverLetter | String | | |
|  └ meta.subjects | [Subjects] | `<subject>` | |
|  └ meta.contributors | [Contributor] | `<contrib>` | |
|  &nbsp;&nbsp;└ contributor.stringName | String | `<string-name>` | |
|  &nbsp;&nbsp;└ contributor.givenNames | String | `<given-names>` | |
|  &nbsp;&nbsp;└ contributor.surname | String | `<surname>` | |
|  &nbsp;&nbsp;└ contributor.aff | String | `<aff>` | |
|  └ meta.authorNote | String | | |
|  └&nbsp;meta.publicationDates | [Publication Dates] | `<pub-date>`| |
|  &nbsp;&nbsp;└&nbsp;publicationDate.type | String | `@pub-type`| epub, ppub, etc. |
|  &nbsp;&nbsp;└&nbsp;publicationDate.date | String | `@iso-8601-date`| date |


## Team

|   | type  | JATS  | comment  |  
|---|---|---|---|
| teamType  | String  |   | e.g. 'reviewers' | 'editors' |
| members | [User] | | |  
| meta | JSON {}  |   | e.g. `{ someMembersId: { status: invited } }`  |
| object | TeamObject | | |
|  └ object.type | String |  | e.g. `ManuscriptVersion`|
|  └ object.id | UUID |  | |


## Decision

|   | type  | JATS  | comment  |  
|---|---|---|---|
| status  | String  |   | e.g. 'accept'  |
| submissionDate  | String  |   | date  |
| content | String  |   |   |
| files | [File]  |   |   |
| user | User  |   | Link to the editor's user  |


## ReviewerInvitation

|   | type  | JATS  | comment  |  
|---|---|---|---|
| invitationDate  | String  |   | date  |
| accepted | Boolean  |   |   |
| responseDate | String  |   |  date |
| user | User  |   |  |
| reminders | [String]  |   | dates |


## Review

|   | type  | JATS  | comment  |  
|---|---|---|---|
| reviewerInvitation | ReviewerInvitation |   | Link to ReviewerInvitation |
| content | String  |   |  |
| files | [File] |   |  |
| public | Boolean | | |
| submissionDate | String  |   | date |
| recommendation | Recommendation | | Link to Recommendation |
 
## Recommendation

| | type  | JATS   | comment |
|---|---|---|---|
| recommendation | String | | accept or revise, etc. |
| user | User | | Link to the user making the recommendation
| recommendationType | String | | e.g. 'review', 'editor'|
| submissionDate | String | | date
| comments | [Comment] | | |
|  &nbsp;&nbsp;└ comment.content | String | | |
|  &nbsp;&nbsp;└ comment.public | String | | |
|  &nbsp;&nbsp;└ files | [File] | | |

## File

| | type | JATS | comment |
|---|---|---|---|
| name | String | | |
| url | String | | |
| size | Number | | |
| type | String | | e.g. 'figure', 'supplementary', 'table' |
| mimeType | String | | e.g. 'image/jpeg', 'application/zip' |


# Removed

## Journal.owners
The owners relationship is non-core/non-semantic and can be modelled with Teams if needed. As such, it will be deprecated/removed.

## Manuscript.originalSubmissionDate
Original submission date is the first versions's submission date, and isn't needed on the container model.

## ManuscriptVersion.meta.contributors.middleName
JATS recommends using `<given-names>` elements instead
